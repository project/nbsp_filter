<?php

namespace Drupal\nbsp_filter\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Handles &nbsp; spaces before/after punctuations.
 *
 * @Filter(
 *   id = "nbsp_filter",
 *   title = @Translation("NBSP Filter"),
 *   description = @Translation("Handles &nbps; (non-breaking spaces in contents), depending on configuration"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "clean_all" = TRUE,
 *     "insert_before" = "?!;:",
 *     "insert_after" = "¿¡",
 *     "insert_narrow_before" = "»",
 *     "insert_narrow_after" = "«"
 *   },
 * )
 */
class NbspFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $default = $this->defaultConfiguration();
    $form['clean_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Transform all &amp;nbps;, &amp;#8239;, ... (non-breaking spaces) with standard spaces"),
      '#default_value' => $this->settings['clean_all'],
      '#description' => $this->t("Remove non-breaking spaces in content and replace with standard spaces (excepted the ones configured below)"),
    ];
    $form['insert_before'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Insert &amp;nbps; (non-breaking space) before'),
      '#default_value' => $this->settings['insert_before'],
      '#description' => $this->t('List of characters to insert a non-breaking space before.'),
    ];
    $form['insert_after'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Insert &amp;nbps; (non-breaking space) after'),
      '#default_value' => $this->settings['insert_after'],
      '#description' => $this->t('List of characters to insert a non-breaking space after.'),
    ];
    $form['insert_narrow_before'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Insert &amp;#8239; (narrow non-breaking space) before'),
      '#default_value' => $this->settings['insert_narrow_before'],
      '#description' => $this->t('List of characters to insert a narrow non-breaking space before.'),
    ];
    $form['insert_narrow_after'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Insert &amp;#8239; (narrow non-breaking space) after'),
      '#default_value' => $this->settings['insert_narrow_after'],
      '#description' => $this->t('List of characters to insert a narrow non-breaking space after.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {

    if ($this->settings['clean_all']) {
      // Remove &nbsp; (HTML non-breaking spaces)
      // Remove UTF-8 encoded non-breaking spaces (\xc2\xa0)
      $text = str_replace(['&nbsp;', "\xc2\xa0"], ' ', $text);
      // Optional: If there are other forms of unbreakable spaces, use regex to target them
      // This regular expression targets any non-breaking space characters
      // in Unicode: U+00A0, U+202F, and similar.
      $text = preg_replace('/[\x{00A0}\x{202F}\x{2009}\x{200A}\x{200B}]/u', ' ', $text);
    }

    // Replace spaces before configured characters with &nbsp;.
    $before_chars = $this->settings['insert_before'];
    if (!empty($before_chars)) {
      $pattern = "/ ([$before_chars])/i";
      $text = preg_replace($pattern, '&nbsp;$1', $text);
    }

    // Replace spaces after configured characters with &nbsp;.
    $after_chars = $this->settings['insert_after'];
    if (!empty($after_chars)) {
      $pattern = "/([$after_chars]) /i";
      $text = preg_replace($pattern, '$1&nbsp;', $text);
    }

    // Replace spaces before configured characters with &#8239;.
    $before_narrow_chars = $this->settings['insert_narrow_before'];
    if (!empty($before_narrow_chars)) {
      $pattern = "/\s(?=[$before_narrow_chars])/";
      $text = preg_replace($pattern, "\xe2\x80\xaf", $text);
    }

    // Replace spaces after configured characters with &#8239;.
    $after_narrow_chars = $this->settings['insert_narrow_after'];
    if (!empty($after_narrow_chars)) {
      $pattern = "/(?<=[$after_narrow_chars])\s/";
      $text = preg_replace($pattern, "\xe2\x80\xaf", $text);
    }

    return new FilterProcessResult($text);
  }

}
